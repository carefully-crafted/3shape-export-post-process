# 3shape export post process

Powershell script to post process (reorder and rename) a 3shape clear aligner stl export.

## Installation (on Windows)

1. Download `3shape-export-post-process.ps1` and save it in a suitable location
2. Create a Shortcut with the follwing target: `C:\Windows\System32\WindowsPowerShell\v1.0\powershell.exe -noLogo -ExecutionPolicy unrestricted -file "<path-to>/3shape-export-post-process.ps1"` e.g. `"Z:\3shape-export-post-process.ps1"`
