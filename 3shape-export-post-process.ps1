# Load the necessary assembly for file dialog
Add-Type -AssemblyName System.Windows.Forms

# Open a folder browser dialog for the user to choose a folder
$folderBrowser = New-Object System.Windows.Forms.FolderBrowserDialog
$folderBrowser.Description = "Select a folder"
$folderBrowser.ShowDialog() | Out-Null

# Store the selected folder's name in the variable folderName
$folderName = [System.IO.Path]::GetFileName($folderBrowser.SelectedPath)

# Create a new folder with the stored name (remove if it exists already)
$newFolderPath = Join-Path -Path $folderBrowser.SelectedPath -ChildPath $folderName
if (Test-Path $newFolderPath) {
    Remove-Item $newFolderPath -Recurse
}
New-Item -Path $newFolderPath -ItemType Directory | Out-Null

# Create OK (Oberkiefer/Maxillary) and UK (unterkiefer/Mandibular) folders
$maxillaryPath = Join-Path -Path $newFolderPath -ChildPath "OK"
$mandibularPath = Join-Path -Path $newFolderPath -ChildPath "UK"
New-Item -Path $maxillaryPath -ItemType Directory | Out-Null
New-Item -Path $mandibularPath -ItemType Directory | Out-Null

$lastSubsetupNumber = 0
# Iterate through folder matching the regex and copy the desired files
Get-ChildItem -Path $folderBrowser.SelectedPath -Directory | Where-Object {
    $_.Name -match "(^\d+-\d+-\d+_\d+-\d+-\d+_Subsetup(\d+)$)"
} | ForEach-Object {
    $subsetupNumber = $matches[2]
    $parsedSubsetupNumber = [int]$subsetupNumber 
    $lastSubsetupNumber = If ($parsedSubsetupNumber -gt $lastSubsetupNumber) {$parsedSubsetupNumber} Else {$lastSubsetupNumber}

    $maxillarySource = Join-Path -Path $_.FullName -ChildPath 'Maxillary.stl'
    $mandibularSource = Join-Path -Path $_.FullName -ChildPath 'Mandibular.stl'
    $maxillaryDest = Join-Path -Path $maxillaryPath -ChildPath ("Maxillary" + $subsetupNumber + ".stl")
    $mandibularDest = Join-Path -Path $mandibularPath -ChildPath ("Mandibular" + $subsetupNumber + ".stl")

    $maxillaryAttachmentSource = Join-Path -Path $_.FullName -ChildPath 'Maxillary_with_attachments.stl'
    $mandibularAttachmentSource = Join-Path -Path $_.FullName -ChildPath 'Mandibular_with_attachments.stl'
    $maxillaryAttachmentDest = Join-Path -Path $maxillaryPath -ChildPath "MaxillaryA.stl"
    $mandibularAttachmentDest = Join-Path -Path $mandibularPath -ChildPath "MandibularA.stl"

    if (Test-Path $mandibularSource) {
        Copy-Item -Path $mandibularSource -Destination $mandibularDest -Force
    }
    
    if (Test-Path $maxillarySource) {
        Copy-Item -Path $maxillarySource -Destination $maxillaryDest -Force
    }

    if (Test-Path $maxillaryAttachmentSource) {
        Copy-Item -Path $maxillaryAttachmentSource -Destination $maxillaryAttachmentDest -Force
    }

    if (Test-Path $mandibularAttachmentSource) {
        Copy-Item -Path $mandibularAttachmentSource -Destination $mandibularAttachmentDest -Force
    }
}

# Number for last file before retainer
$lastNumber = $lastSubsetupNumber + 1

# Get last file before retainer
Get-ChildItem -Path $folderBrowser.SelectedPath -Directory | Where-Object {
    $_.Name -match "(^\d+-\d+-\d+_\d+-\d+-\d+$)"
} | ForEach-Object {

    $maxillarySource = Join-Path -Path $_.FullName -ChildPath 'Maxillary.stl'
    $mandibularSource = Join-Path -Path $_.FullName -ChildPath 'Mandibular.stl'
    $maxillaryDest = Join-Path -Path $maxillaryPath -ChildPath ("Maxillary" + $lastNumber + ".stl")
    $mandibularDest = Join-Path -Path $mandibularPath -ChildPath ("Mandibular" + $lastNumber + ".stl")

    if (Test-Path $mandibularSource) {
        Copy-Item -Path $mandibularSource -Destination $mandibularDest -Force
    }
    
    if (Test-Path $maxillarySource) {
        Copy-Item -Path $maxillarySource -Destination $maxillaryDest -Force
    }
}

# Get retainer
Get-ChildItem -Path $folderBrowser.SelectedPath -Directory | Where-Object {
    $_.Name -match "(^\d+-\d+-\d+_\d+-\d+-\d+_Retainer\d+$)"
} | ForEach-Object {
    $maxillarySource = Join-Path -Path $_.FullName -ChildPath 'Maxillary.stl'
    $maxillaryDest = Join-Path -Path $maxillaryPath -ChildPath "MaxillaryR.stl"
    $mandibularSource = Join-Path -Path $_.FullName -ChildPath 'Mandibular.stl'
    $mandibularDest = Join-Path -Path $mandibularPath -ChildPath "MandibularR.stl"

    if (Test-Path $mandibularSource) {
        Copy-Item -Path $mandibularSource -Destination $mandibularDest -Force
    }
    
    if (Test-Path $maxillarySource) {
        Copy-Item -Path $maxillarySource -Destination $maxillaryDest -Force
    }
}

#Read-Host -Prompt "Press Enter to exit"
